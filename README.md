GPIO Controller für Adventskranz
====================================

Dieses Git-Repository enthält alle notwendigen Quellcodes zur Erstellung des
GPIO Controllers für eine Adventskranzsteuerung auf Basis eines 
Raspberry PI Einplatinen-Computers. 


Container Image laden und starten
---------------------------------

Das fertige Docker-Image zum Herunterladen aus dem Docker-Registry finden Sie 
hier:

```
docker pull legendlime/adventskranz-controller:1.0
```

Nach Download des Docker-Image können Sie den GPIO Controller auf einem 
Raspberry PI mit Hypriotos Betriebssystem folgendermaßen starten:

```
docker run -d --name gpio-wreath -v /sys:/sys \
  -e MQTT_IP=<public IP des Kubernets Mosquitto Service> \
  --restart always \
  legendlime/adventskranz-controller:1.0
```

Erklärungen:
- Das /sys Filesystem des Raspberry PI muss als Volume für den Container freigegeben werden, da hierüber die GPIO Pins gesteuert werden
- Umgebungsvariablen: `MQTT_IP`: IP-Adresse des MQTT Message Brokers. Dies ist die öffentliche IP Adresse des Cloud LoadBalancers, die dem Mosquitto Service zugewiesen wurde
- Der Container wird immer automatisch neu gestartet, sollte er aus irgendeinem Grund mal abstürzen (--restart Option)


Container Image selbst bauen
----------------------------
Wer sich lieber sein eigenes Docker-Image basteln will, dem sei der beigefügte Dockerfile im Repository ans Herz gelegt. Das Image kann wie folgt erzeugt werden:

```
docker build -t <my-image-path> -f Dockerfile .
```

Das Kommando muss im gleichen Verzeichnis ausgeführt werden, in dem sich `Dockerfile`und `gpio-wreath.js`befinden.


Schaltplan
----------------------------
Die Schaltung, welche über die GPIOs des Raspberry PI gesteuert wird, ist ausgesprochen einfach. Es gibt je nach Konfiguration eine Anzahl von Ausgängen für das Ein- und Ausschalten der LEDs des Adventskranzes sowie eine optionale Schaltung für einen Hauptschalter (Taster), mit dem, wenn es mal schnell gehen muss, und das Handy ist nicht zur Hand, alle Aktoren aus oder auch wieder eingeschaltet werden können.
Hier der Schaltplan, der für den GPIO Controller in der Standard-Konfiguration verwendet wurde:

![Schaltplan Adventskranz](adventskranz-schema.png)

![Breadboard](wreath-fritzing.png)

Viel Spaß,
Thomas Kautenburger