FROM arm32v7/node:10.23.0

COPY qemu-arm-static /usr/bin

WORKDIR /app
ADD gpio-wreath.js /app/gpio-wreath.js

RUN npm install mqtt
RUN npm install onoff

RUN rm /usr/bin/qemu-arm-static

ENTRYPOINT ["node", "/app/gpio-wreath.js"]