// GPIO controller
// Thomas Kautenburger, 03.10.2018

var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var fs = require('fs');

// GPIO object array for REST controller
var GPIOs = [];

// sanity check for min and max  gpio numbers
const GPIO_MIN = 2
const GPIO_MAX = 26;

// GPIO object for master switch input
var master_in_gpio;
// GPIO object for master switch output
var master_out_gpio;

// GPIO pin numbers for main switch input and output
const GPIO_SWITCH_IN  = 26;
const GPIO_SWITCH_OUT  = 19;

// check interval for master switch in milliseconds
const CHECK_INTERVAL = 2000;

// current state of master switch, start with on an last configuration set
var MASTER_SWITCH = true;

// Default configuration, if the config file is not present, the
// GPIO controller comes up with this default configuration

// array for default configuration controls
var array = [];

// default configuration
advent1 = new Object();
advent1.name = "1. Advent";
advent1.pin = 22;
advent1.status = 0;
array.push(advent1);

advent2 = new Object();
advent2.name = "2. Advent";
advent2.pin = 23;
advent2.status = 0;
array.push(advent2);

advent3 = new Object();
advent3.name = "3. Advent";
advent3.pin = 24;
advent3.status = 0;
array.push(advent3);

advent4 = new Object();
advent4.name = "4. Advent";
advent4.pin = 25;
advent4.status = 0;
array.push(advent4);

var defaultConfig = new Object();
defaultConfig.title = "Adventskranz";
defaultConfig.controls = array;

// MQTT server public IP address
var MQTT_IP = process.env.MQTT_IP;

// Get configuration from file or if not existent use default config
var globConfig = defaultConfig;

// Create GPIO objects
gpioAllInit();

// Set initial state for all outgoing GPIOs
globConfig.controls.forEach(function(element){
  gpioSet(element.pin, element.status);
});


/*****************************************************
 * GPIO watcher for main on/off switch
 *****************************************************/

master_in_gpio.watch((err, value) => {
  if (err) {
    console.log("Received error while watching master switch: " + err.toString());
  }
  // toggle control light and master switch
  gpioToggle(master_out_gpio);
  MASTER_SWITCH = !MASTER_SWITCH;
});


/*****************************************************
 * interval handling
 *****************************************************/

// check every N second the status of the main switch
var intervalId;
intervalId = setInterval( function(){
  if (MASTER_SWITCH  === false) {
    // Master switch turned down
    globConfig.controls.forEach( function(element) {
      if (element.status === 1) {
        gpioDown(element.pin);
      }
      client.publish(TOPIC_CANDLE_OFF, element.pin.toString());
    });
  } else if (MASTER_SWITCH  === true) {
    // Master switch turned up, get all pins up that where up due to
    // latest configuration seetings
    globConfig.controls.forEach( function(element) {
      if (element.status === 1) {
        // GPIO is high according to config, so get it back up
        gpioUp(element.pin);
        client.publish(TOPIC_CANDLE_ON, element.pin.toString());
      }
    });
  }
}, CHECK_INTERVAL);


/*****************************************************
 * MQTT handling
 *****************************************************/

// MQTT topics for switching events
var TOPIC_CANDLE_OFF = 'gpc/candleOff';
var TOPIC_CANDLE_ON = 'gpc/candleOn';
var TOPIC_REFRESH = 'gpc/refresh';

var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://' + MQTT_IP);

// Ask for candle refresh during startup
client.publish(TOPIC_REFRESH, 'refresh');

// Listen to light on and light off events
client.on('connect', function() {
  client.subscribe(TOPIC_CANDLE_OFF, function(err) {
    if (!err) {
      console.log("MQTT listen on topic ", TOPIC_CANDLE_OFF);
    } else {
      console.log("Error while connecting to MQTT broker: ", err);
    }
  })
  client.subscribe(TOPIC_CANDLE_ON, function(err) {
    if (!err) {
      console.log("MQTT listen on topic ", TOPIC_CANDLE_ON);
    } else {
      console.log("Error while connecting to MQTT broker: ", err);
    }
  })
})

client.on('message', function (topic, message) {
  if (topic === TOPIC_CANDLE_OFF) {
	  // received candlle on event from MQTT
    console.log('Set GPIO ' + message + ' to DOWN');
	  gpioDown(message);
  } else if (topic === TOPIC_CANDLE_ON) {
	  // received candle off event from MQTT
    console.log('Set GPIO ' + message + ' to UP');
	  gpioUp(message)
  }
})

function gpioDown(pin) {
  if (MASTER_SWITCH && pin >= GPIO_MIN && pin <= GPIO_MAX) {
    const result = GPIOs.find(element => element.pin == parseInt(pin));
    if (result != undefined) {
      gpioLow(result.gpio);
    }
  }
}

function gpioUp(pin) {
  if (MASTER_SWITCH && pin >= GPIO_MIN && pin <= GPIO_MAX) {
    const result = GPIOs.find(element => element.pin == parseInt(pin));
    if (result != undefined) {
      gpioHigh(result.gpio);
    }
  }
}

function gpioSet(pin, status) {
  if (MASTER_SWITCH && pin >= GPIO_MIN && pin <= GPIO_MAX) {
    const result = GPIOs.find(element => element.pin == parseInt(pin));
    if (result != undefined) {
      if (status == 0) {
        gpioLow(result.gpio);
      } else {
	      gpioHigh(result.gpio);
      }
    }
  }
}


/*****************************************************
 * General helper functions
 *****************************************************/

// Clean up when stopping the container
function unexportOnClose() { //function to stop blinking
  console.log("GPIO controller is going down. Turning down GPIOs");
  GPIOs.map(obj => {
    gpioLow(obj.gpio);
    obj.gpio.unexport();
  });
  gpioLow(master_out_gpio);
  master_out_gpio.unexport();
  master_in_gpio.unexport();
  client.end();
  process.exit(0);
}

// Handle some signals that shutdown the service
process.on('SIGINT', unexportOnClose); // function to run when user closes with ctrl+c
process.on('SIGTERM', unexportOnClose); // function to run when process gets TERM signal


// Set GPIO with the given GPIO object to 1
function gpioHigh(gpio) {
  if (gpio.readSync() == 0) {
    gpio.writeSync(1);
  }
}

// Set GPIO with the given GPIO object to 0
function gpioLow(gpio) {
  if (gpio.readSync() == 1) {
    gpio.writeSync(0);
  }
}

// Toggle GPIO
function gpioToggle(gpio) {
  gpio.writeSync(gpio.readSync() ^1);
}

// Get the status from the given GPIO object
function gpioStatus(gpio) {
  let state = gpio.readSync();
  return state;
}

// Initialize the GPIO with the GPIO number to input or output contact
// and return the GPIO object
function gpioInit(pin, direction) {
  return new Gpio(pin, direction);
}

// Initialiye all GPIO object received from the configuration and
// Initialize the contacts for master switch
function gpioAllInit() {
  globConfig.controls.forEach( function(element) {
    let gpioObj = new Object();
    gpioObj.pin = element.pin;
    gpioObj.gpio = gpioInit(element.pin, 'out');
    GPIOs.push(gpioObj);
  });

  // set the master in switch
  master_in_gpio  = new Gpio(GPIO_SWITCH_IN, 'in', 'rising', {debounceTimeout: 10});

  // set the master light out gpio and we start with ON
  master_out_gpio = gpioInit(GPIO_SWITCH_OUT, 'out');
  gpioHigh(master_out_gpio);
}
